#!/bin/bash

### Initial set up for fedora install ###

### Add RPM Fusion

echo "ADDING RPM FUSION TO SYSTEM"

sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

echo "INSTALL COMPLETE"
sleep 3s

### Adding multimedia Codecs



### RPM Adding ATOM IDE

echo "ADDING ATOM"

sudo rpm --import https://packagecloud.io/AtomEditor/atom/gpgkey

sudo sh -c 'echo -e "[Atom]\nname=Atom Editor\nbaseurl=https://packagecloud.io/AtomEditor/atom/el/7/\$basearch\nenabled=1\ngpgcheck=0\nrepo_gpgcheck=1\ngpgkey=https://packagecloud.io/AtomEditor/atom/gpgkey" > /etc/yum.repos.d/atom.repo'

echo "ATOM ADDED"
sleep 3s

### Update system ###

echo "UPGRADING SYSTEM"

sudo dnf upgrade

echo "UPGRADE COMPLETE"
sleep 3s

### Improve mirrors ###



### Install programs ###

echo "INSTALLING PROGRAMS"

sudo dnf install -y neofetch
sudo dnf install -y exfat-utils 
sudo dnf install -y fuse-exfat
sudo dnf install -y htop
sudo dnf install -y gnome-tweaks
sudo dnf install -y steam
sudo dnf install -y lutris
sudo dnf install -y dxvk
sudo dnf install -y meld
sudo dnf install -y supertuxkart
sudo dnf install -y supertux
sudo dnf install -y geany geany-plugins-*
sudo dnf install -y gnome-shell-theme gnome-shell-theme-yaru gnome-shell-theme-selene gnome-shell-theme-flat-remix
sudo dnf install -y mlocate
sudo dnf install -y gparted
sudo dnf install -y mediawriter
sudo dnf install -y ffmpeg

echo "INSTALL COMPLETE"
sleep 3s

### Update database for locate

echo "UPDATING DATABASE FOR MLOCATE"

sudo updatedb

echo "UPDATE COMPLETE"
sleep 3s

### Remove programs ###

echo "REMOVING PROGRAMS"

sudo dnf autoremove rhythmnbox
sudo dnf autoremove cheese
sudo dnf autoremove brasero
#sudo dnf autoremove 

echo "REMOVAL COMPLETE"
sleep 3s

### Check to see if bashrc file is present. If not, it'll create it

echo "CHECKING FOR FILES AND ADDING, WHERE NECESSARY"

if [[ ! -f ~/.bashrc ]]; then
	touch ~/.bashrc
fi


if [[  ! -d ~./icons ]]; then
	touch ~./icons
fi

if [[ ! -d ~./themes ]]; then
	touch ~./themes
fi

echo "OPERATION COMPLETE"
sleep 3s

### Amend bashrc file ###

echo "ADDING TO BASH RC FILE"

echo "" >> ~/.bashrc
echo "neofetch" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "### ALIASES ###" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "alias ..='cd ..'" >> ~/.bashrc
echo "alias se='search'" >> ~/.bashrc
echo "alias in='install'" >> ~/.bashrc
echo "alias rm='remove'" >> ~/.bashrc
echo "alias md='mkdir'" >> ~/.bashrc
echo "alias rd='rmdir'" >> ~/.bashrc
echo "alias l='ls -alFh --color=auto'" >> ~/.bashrc

echo "OPERATION COMPLETE"
sleep 3s

echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"
